#!/usr/bin/env python3
"""Mitmproxy script to replace cloudflare response content
with content fetched with Selenium
"""

import tempfile
from mitmproxy import http  # type: ignore
from selenium import webdriver  # type: ignore
from selenium_stealth import stealth  # type: ignore


options = webdriver.ChromeOptions()
options.add_argument("start-maximized")
options.add_argument("--headless=new")
options.add_experimental_option("excludeSwitches", ["enable-automation"])
options.add_experimental_option('useAutomationExtension', False)
driver = webdriver.Chrome(options=options)

stealth(driver,
        languages=["en-US", "en"],
        vendor="Google Inc.",
        platform="Win32",
        webgl_vendor="Intel Inc.",
        renderer="Intel Iris OpenGL Engine",
        fix_hairline=True,
        )

def response(flow: http.HTTPFlow):
    """Replace response content with content fetched by Selenium
    if the request is served by cloudflare
    """
    if 'server' in flow.response.headers and \
        flow.response.headers['server'] == 'cloudflare' and \
        str(flow.response.status_code).startswith('4'):
        driver.get(flow.request.pretty_url)
        page_source = driver.page_source

        with tempfile.NamedTemporaryFile(prefix='selenium-get_') as temp:
            temp.write(page_source.encode('utf8'))
            temp.seek(0)
            flow.response.content = temp.read()
            temp.close()
