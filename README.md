# Selenium proxy for Cloudflare

If you have a software that needs to fetch some Cloudflare-protected pages, this proxy refetches the pages if Cloudflare blocks the request.

## Installation

```bash
cd /opt
git clone https://framagit.org/luc/selenium-proxy-for-cloudflare.git selenium-for-cloudflare
sudo apt install chromium python3-virtualenv
virtualenv venv
. venv/bin/activate
pip install -r requirements.txt
```

If you want to start the proxy with the user `www-data`:
```
sudo mkdir ~www-data/.mitmproxy/
sudo chown www-data: ~www-data/.mitmproxy/
```

## Start the proxy

```bash
/opt/selenium-for-cloudflare/venv/bin/mitmdump --listen-port 8000 --listen-host 127.0.0.1 --mode regular -s modifybody.py
```

With systemd, create `/etc/systemd/system/selenium-for-cloudflare.service`:
```ini
[Unit]
Description=Selenium proxy for Cloudflare

[Service]
User=www-data
ExecStart=/opt/selenium-for-cloudflare/venv/bin/mitmdump --listen-port 8000 --listen-host 127.0.0.1 --mode regular -s modifybody.py
Restart=always

[Install]
WantedBy=default.target
```

Then:
```
systemctl daemon-reload
systemctl enable --now selenium-for-cloudflare.service
```

## Test it

NB: you need to add the CA certificate of the proxy to the client you use.

```bash
curl --cacert ~/.mitmproxy/mitmproxy-ca-cert.pem -x http://127.0.0.1:8000 https://a_page_protected_by_cloudflare.example.com
# Compare with:
curl https://a_page_protected_by_cloudflare.example.com
```

## License

Licensed under the terms of the Gnu GPLv3. See [LICENSE](LICENSE)
